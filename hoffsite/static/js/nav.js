//  Creating our button for smaller screens
var menuElements = document.getElementById('menu');
menuElements.insertAdjacentHTML('afterBegin','<button type="button" id="menutoggle" class="navtoggle" aria-hidden="true"><i aria-hidden="true" class="icon-menu"> </i> Menu</button>');

$('#menutoggle').click(function(e) {
  $('#menutoggle ~ ul').toggleClass('active');
});

$(document).click(function(e) {
  var container = $('#menu');
  if (!container.is(e.target)  // if the target of the click isn't the container...
      && container.has(e.target).length === 0  // ... nor a descendant of the container
      && $('#menutoggle').css('display') === 'block') {
    $('#menutoggle ~ ul').removeClass('active');
  }
});

toggleSubmenu = function(e) {
  $el = $(this).children().filter('ul');
  $el.toggleClass('active');
};

$('.no-touch nav > ul > li:not( .empty )').on('mouseenter mouseleave', toggleSubmenu);
$('.touch nav > ul > li:not( .empty )').on('click', toggleSubmenu);
