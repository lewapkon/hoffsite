from django.contrib import admin
from django import forms
import models
from django_summernote.widgets import SummernoteInplaceWidget

# Register your models here.

class PostForm(forms.ModelForm):
	class Meta:
		models = models.Post
		widgets = {
			'text': SummernoteInplaceWidget
		}

class PostAdmin(admin.ModelAdmin):
	form = PostForm
	prepopulated_fields = {'slug': ('title',)}
	exclude = ('author','site')

	def save_model(self, request, obj, form, change):
		obj.author = request.user
		obj.save()

class PageForm(forms.ModelForm):
    class Meta:
        models = models.Page
        widgets = {
            'text': SummernoteInplaceWidget
        }

class PageAdmin(admin.ModelAdmin):
    form = PageForm
    prepopulated_fields = {'slug': ('title',)}
    exclude = ('site',)

class CategoryForm(forms.ModelForm):
    class Meta:
        models = models.Category

class CategoryAdmin(admin.ModelAdmin):
    form = CategoryForm
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Post, PostAdmin)
admin.site.register(models.Page, PageAdmin)