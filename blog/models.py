from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.flatpages.models import FlatPage
from django.utils.text import slugify
from datetime import datetime

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=40, unique=True)
    icon = models.CharField(max_length=8, unique=True)
    order = models.PositiveIntegerField(unique=True)

    def save(self):
        if not self.slug:
            self.slug = slugify(unicode(self.name))
        super(Category, self).save()

    def get_absolute_url(self):
        return "/category/{0}".format(self.slug)

    def get_pages(self):
        return Page.objects.filter(category=self)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'categories'
        ordering = ['order']    

class Post(models.Model):
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField(default=datetime.now)
    text = models.TextField()
    slug = models.SlugField(max_length=40, unique=True)
    author = models.ForeignKey(User)
    site = models.ForeignKey(Site, default=1)

    def get_absolute_url(self):
        return '/news/{0}/{1}/{2}'.format(self.pub_date.year, self.pub_date.month, self.slug)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-pub_date']

class Page(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    slug = models.SlugField(max_length=40, unique=True)
    order = models.PositiveIntegerField(unique=True)
    site = models.ForeignKey(Site, default=1)
    category = models.ForeignKey(Category)

    def get_absolute_url(self):
        return '/{0}/{1}'.format(self.category.slug, self.slug)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']
