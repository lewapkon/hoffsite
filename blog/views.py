from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.contrib.syndication.views import Feed
from blog.models import Category, Post, Page

# Create your views here.
class PostListView(ListView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context

class PostView(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super(PostView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context

class PageView(DetailView):
    model = Page

    def get_context_data(self, **kwargs):
        context = super(PageView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context

class CategoryListView(ListView):
	def get_queryset(self):
		slug = self.kwargs['slug']
		try:
			category = Category.objects.get(slug=slug)
			return Post.objects.filter(category=category)
		except Category.DoesNotExist:
			return Post.objects.none()

class PostsFeed(Feed):
	title = 'RSS feed - posts'
	link = 'feeds/posts/'
	description = 'RSS feed - blog posts'

	def items(self):
		return Post.objects.order_by('-pub_date')

	def item_title(self, item):
		return item.title

	def item_description(self, item):
		return item.text
