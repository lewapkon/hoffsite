from django.conf.urls import patterns, url
from django.views.generic import ListView
from blog.models import Post, Category, Page
from blog.views import PostListView, PostView, PageView, CategoryListView, PostsFeed

urlpatterns = patterns('',
    # Index
    url(r'^(?P<page>\d+)?/?$', PostListView.as_view(
        model=Post,
        paginate_by=5
        )),

    # Individual posts
    url(r'^news/(?P<pub_date__year>\d{4})/(?P<pub_date__month>\d{1,2})/(?P<slug>[a-zA-Z0-9-]+)/?$', PostView.as_view(
    	model=Post
    	)),

    # Individual pages
    url(r'^(?P<category>[a-zA-Z0-9-]+)/(?P<slug>[a-zA-Z0-9-]+)/?$', PageView.as_view(
        model = Page
        )),

    # Post RSS Feed
    url(r'^feeds/posts/$', PostsFeed()),
)